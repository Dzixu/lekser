﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace Lekser
{
    enum TYPE
    {
        BLAD,
        SYMBOL,
        IDENTYFIKATOR,
        LICZBA_CALKOWITA,
        LICZA_ZMIENNOPRZECINKOWA
    }

    class Program
    {
        public static string FileReadStream { get; set; }
        public static List<string> fileRead { get; set; }
        public static IList<LeksElement> outPut { get; set; }

        static void Main(string[] args)
        {
            fileRead = new List<string>();
            outPut = new List<LeksElement>();
            //StreamReader file = new StreamReader("text.txt");

            var fileName = "text.txt";
            if (args.Any())
            {
                fileName = args[0].ToString();
            }

            using (StreamReader file = new StreamReader(fileName))
            {
                FileReadStream = "";

                //pobieranie tekstu
                while (file.Peek() >= 0)
                {
                    var l = file.ReadLine();
                    FileReadStream += l + "\n";
                    fileRead.Add(l);
                }

                //usuwanie zbednych spacji
                FileReadStream = FileReadStream.Trim();
                Console.WriteLine($"FileReadStream file:");
                Console.WriteLine($"{FileReadStream}");
                Console.WriteLine();

                //warunki przypisywania typu
                var wOrdOrDotReg = new Regex(@"\w|\d|\.");
                var symbolsReg = new Regex(@"\+|\-|\*|\/|\(|\)");

                //wydzielanie slow i symboli wraz z ich typem
                var word = "";
                int line = 1;
                int position = 1;
                try
                {
                    foreach (var letter in FileReadStream)
                    {
                        var matchLetter = wOrdOrDotReg.Match(letter.ToString());
                        var matchSymbol = symbolsReg.Match(letter.ToString());

                        if (wOrdOrDotReg.Match(letter.ToString()).Success)
                        {
                            word += letter;
                            position++;
                        }
                        else if (symbolsReg.Match(letter.ToString()).Success)
                        {
                            if (word.Length != 0)
                            {
                                checkWord(word, line, position);
                                word = "";
                            }
                            outPut.Add(new LeksElement(letter.ToString(), TYPE.SYMBOL, line, position));
                            position++;
                        }
                        else if (letter == '\n')
                        {
                            if (word.Length != 0)
                            {
                                checkWord(word, line, position);
                                word = "";
                            }
                            position = 1;
                            line++;
                        }
                        else if (letter == ' ')
                        {
                            if (word.Length != 0)
                            {
                                checkWord(word, line, position);
                                word = "";
                                position++;
                            }
                        }
                        else
                        {
                            if (word.Length != 0)
                            {
                                checkWord(word, line, position);
                                word = "";
                            }
                            outPut.Add(new LeksElement(letter.ToString(), TYPE.BLAD, line, position));
                            position++;
                            throw new Exception("Znaleziono niepoprawny ciag znakow!");
                        }
                    }

                    if (word != "")
                    {
                        checkWord(word, line, position);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(e.Message);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                finally
                {

                    foreach (var item in outPut)
                    {
                        if (item.Type != TYPE.BLAD)
                        {
                            Console.WriteLine($"\t> {item.Expresion} [{item.Type.ToString()}],  ");
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($"\t> {item.Expresion} [{item.Type.ToString()}],  ");
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }

                    var errors = outPut.Where(x => x.Type == TYPE.BLAD);

                    if (!outPut.Any())
                    {
                        Console.WriteLine(" - Brak elementow do wyswielenie");
                    }

                    if (errors.Any())
                    {
                        Console.WriteLine();
                        Console.WriteLine("Wyjątki: ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        foreach (var item in errors)
                        {
                            Console.WriteLine($"\t> Element '{item.Expresion}' - Niepoprawny ciag znakow  - Linia: {item.Row}, Pozycja: {item.Column}");
                        }
                        Console.WriteLine();
                        Console.WriteLine("Done with errors...");
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("Done!");
                    }
                }
                Console.ForegroundColor = ConsoleColor.White;
            }


            Console.ReadLine();
        }

        private static void checkWord(string word, int row, int column)
        {
            var intigerReg = new Regex(@"^(?:\d*)$");
            var doubleReg = new Regex(@"^(?:\d+)\.(?:\d+)$");
            var doubleErrorReg = new Regex(@"^(?:.*)\.(?:.*)$");
            var startWithNumReg = new Regex(@"^(?:\d..*)$");

            if (intigerReg.Match(word).Success)
            {
                outPut.Add(new LeksElement(word, TYPE.LICZBA_CALKOWITA, row, column));
            }
            else if (doubleReg.Match(word).Success)
            {
                outPut.Add(new LeksElement(word, TYPE.LICZA_ZMIENNOPRZECINKOWA, row, column));
            }
            else if (startWithNumReg.Match(word).Success)
            {
                checkWordStartedWithNum(word, row, column);
            }
            else if (doubleErrorReg.Match(word).Success)
            {
                var temp = word.Split(new string[] {"."}, 2, StringSplitOptions.RemoveEmptyEntries);
                if (temp.Count() == 0)
                {
                    outPut.Add(new LeksElement(word, TYPE.BLAD, row, column - word.Count()));
                    throw new Exception("Znaleziono niepoprawny ciag znakow!");
                }
                else
                {
                    checkWord(temp[0], row, column);
                }

                if (temp.Count() > 1)
                {
                    outPut.Add(new LeksElement("."+temp[1], TYPE.BLAD, row, column - word.Count()));
                    throw new Exception("Znaleziono niepoprawny ciag znakow!");
                }
                else
                {
                    outPut.Add(new LeksElement(".", TYPE.BLAD, row, column - word.Count()));
                    throw new Exception("Znaleziono niepoprawny ciag znakow!");
                }
                throw new Exception("Znaleziono niepoprawny ciag znakow!");
            }
            else
            {
                outPut.Add(new LeksElement(word, TYPE.IDENTYFIKATOR, row, column));
            }
        }

        private static void checkWordStartedWithNum(string word, int row, int column)
        {
            var numReg = new Regex(@"\d");
            var dotReg = new Regex(@"\.");
            string num = "";
            string text = "";
            bool isNum = true;
            bool isLastDot = true;

            foreach (var letter in word)
            {
                if (numReg.Match(letter.ToString()).Success && isNum == true)
                {
                    num += letter;
                    isLastDot = false;
                }
                else if(dotReg.Match(letter.ToString()).Success && isNum == true)
                {
                    isLastDot = true;
                    num += letter;
                }
                else
                {
                    isNum = false;
                    text += letter;
                }
            }
            if (num.Count() < word.Count())
            {
                if (isLastDot == false)
                {
                    checkWord(num, row, column);
                    checkWord(text, row, column);
                }
                else
                {
                    checkWord(num.Split(new string[] { "." }, 2, StringSplitOptions.RemoveEmptyEntries)[0], row, column);
                    outPut.Add(new LeksElement("." + text, TYPE.BLAD, row, column));
                    throw new Exception("Znaleziono niepoprawny ciag znakow!");
                }
            }
            else
            {
                checkWord(num.Remove(num.Count() - 1), row, column);
                outPut.Add(new LeksElement(num[num.Count() - 1].ToString(), TYPE.BLAD, row, column));
            }
        }

        public class LeksElement
        {
            public string Expresion { get; set; }
            public TYPE Type { get; set; }
            public int Row { get; set; }
            public int Column { get; set; }

            public LeksElement(string exp, TYPE type, int row, int column)
            {
                Expresion = exp;
                Type = type;
                Row = row;
                Column = column;
            }
        }
    }
}
